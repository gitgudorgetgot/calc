export interface ExtendedOption {
    type: string;
    disallowed?: boolean;
    disallowedReason?: string;
}
