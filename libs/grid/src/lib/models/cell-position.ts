export enum CellPosition {
  TopLeft = 'TopLeft',
  Top = 'Top',
  TopRight = 'TopRight',
  Right = 'Right',
  BottomRight = 'BottomRight',
  Bottom = 'Bottom',
  BottomLeft = 'BottomLeft',
  Left = 'Left'
}
