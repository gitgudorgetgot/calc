import { GridCellDisplayPreset } from './grid-cell-display-preset';

export interface GridCellConfig {
  content: any;
  preset?: GridCellDisplayPreset;
}
