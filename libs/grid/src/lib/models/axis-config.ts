
export interface AxisConfig {
    indices: string[];
    prefix: string;
}
