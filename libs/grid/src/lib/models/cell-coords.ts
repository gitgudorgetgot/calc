export interface CellCoords {
  x: number;
  y: number;
}
