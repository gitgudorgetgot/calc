export { DocPage } from './lib/components/doc-page/doc-page';

export { useDocs } from './lib/hooks/use-docs';
